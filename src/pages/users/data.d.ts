// * 单个用户数据
export interface SingleUserType {
  id: Number;
  name: String;
  email: String;
  create_time: String;
  update_time: String;
  status: Number;
}

// * 单个用户结构
export interface UserState {
  data: SingleUserType[];
  meta: {};
}


// * 表格传值数据
export interface FormValues {
  [name: string]: any;
}
