import request, { extend } from 'umi-request';
import { message } from 'antd';
import { FormValues } from './data';

const errorHandler = function (error: any) {
  const codeMap = {
    '021': 'An error has occurred',
    '022': 'It’s a big mistake,',
  };

  if (error.response) {
    console.log(error.response.status); // code 5xx，4xx之类的
    console.log(error.response.headers); // headers 头部
    console.log(error.data); // body 主体内容
    console.log(error.request); // 请求头
    // console.log(codeMap[error.data.status]);

    if (error.response.status > 400) {
      message.error('错误代码：' + error.response.status);
    }
  } else {
    console.log(error.message);
  }
  throw error;
};

const extendRequest = extend({ errorHandler });

// todo 获取数据
export const getRemoteList = async (per_page: number, page: number) => {
  return extendRequest(`/api/users?page=${page}&per_page=${per_page}`, {
    method: 'get',
    params: {}, //使用 page 进行查询, per_page 表示查询出来的记录个数
  })
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log(error);
    });
};

// todo 进行编辑
export const editRecord = async (id: number, values: FormValues) => {
  return extendRequest(`/api/users/${id}`, {
    method: 'put',
    data: values,
  })
    .then(response => {
      // todo 服务器编辑成功不会给你数据，自己提供数据
      return {
        state: 0,
        message: '修改成功',
      };
    })
    .catch(error => {
      // return {
      //   state: 1,
      //   message: '操作失败',
      // };

      return errorHandler(error);
    });
};

// todo 进行新增
export const addRecord = async (values: FormValues) => {
  return extendRequest(`/api/users`, {
    method: 'post',
    data: values,
  })
    .then(response => {
      return {
        state: 0,
        message: '添加成功',
      };
    })
    .catch(error => {
      return {
        state: 1,
        message: '操作失败',
      };
    });
};

// todo 进行删除
export const deleteRecord = async (id: number) => {
  return extendRequest(`/api/users/${id}`, {
    method: 'delete',
    data: {},
  })
    .then(response => {
      // todo 服务器删除成功不会给你数据，自己提供数据
      return {
        state: 0,
        message: '删除成功',
      };
    })
    .catch(error => {
      // 直接返回false也是可以的，但是model不要做处理
      return {
        state: 1,
        message: '操作失败',
      };
    });
};
